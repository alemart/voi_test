/**
 * Copyright (c) 2017 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 *
 * @defgroup usbd_ble_uart_example main.c
 * @{
 * @ingroup  usbd_ble_uart_example
 * @brief    USBD CDC ACM over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service
 * and USBD CDC ACM library.
 * This application uses the @ref srvlib_conn_params module.
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_db_discovery.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_scan.h"
#include "app_timer.h"
#include "ble_nus.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "bsp_btn_ble.h"


#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "nrf_drv_usbd.h"
#include "nrf_drv_clock.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_power.h"

#include "app_error.h"
#include "app_util.h"
#include "app_usbd_core.h"
#include "app_usbd.h"
#include "app_usbd_string_desc.h"
#include "app_usbd_cdc_acm.h"
#include "app_usbd_serial_num.h"

#define CENTRAL_SCANNING_LED        BSP_BOARD_LED_2                /**< Scanning LED will be blinking when the device is scanning. */
#define CENTRAL_MATCH_LED           BSP_BOARD_LED_1                /**< MATCH LED will be on when the device found a matching Name. */

#define LED_BLINK_INTERVAL 500 /**< 500 ms on 500 ms off */

APP_TIMER_DEF(m_blink_scan); /**< Timer for the blinking the LED when the device is scanning. */

/**
 * @brief App timer handler for blinking the LED.
 *
 * @param p_context LED to blink.
 */
void blink_handler(void * p_context)
{
    bsp_board_led_invert((uint32_t) p_context);
}

// USB DEFINES START
static void cdc_acm_user_ev_handler(app_usbd_class_inst_t const * p_inst,
                                    app_usbd_cdc_acm_user_event_t event);

#define CDC_ACM_COMM_INTERFACE  0
#define CDC_ACM_COMM_EPIN       NRF_DRV_USBD_EPIN2

#define CDC_ACM_DATA_INTERFACE  1
#define CDC_ACM_DATA_EPIN       NRF_DRV_USBD_EPIN1
#define CDC_ACM_DATA_EPOUT      NRF_DRV_USBD_EPOUT1

static char m_rx_buffer[BLE_NUS_MAX_DATA_LEN]; /**< RX buffer for the USB. */
static char m_tx_buffer[BLE_NUS_MAX_DATA_LEN]; /**< TX buffer for the USB */

/** @brief CDC_ACM class instance */
APP_USBD_CDC_ACM_GLOBAL_DEF(m_app_cdc_acm,
                            cdc_acm_user_ev_handler,
                            CDC_ACM_COMM_INTERFACE,
                            CDC_ACM_DATA_INTERFACE,
                            CDC_ACM_COMM_EPIN,
                            CDC_ACM_DATA_EPIN,
                            CDC_ACM_DATA_EPOUT,
                            APP_USBD_CDC_COMM_PROTOCOL_AT_V250);

// USB DEFINES END

// BLE DEFINES START
#define APP_BLE_CONN_CFG_TAG            1                                   /**< A tag identifying the SoftDevice BLE configuration. */
#define APP_BLE_OBSERVER_PRIO           3                                   /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define SCAN_INTERVAL                   0x00A0                              /**< Determines scan interval in units of 0.625 millisecond. */
#define SCAN_WINDOW                     0x0050                              /**< Determines scan window in units of 0.625 millisecond. */
#define SCAN_DURATION                   0x0000                              /**< Timout when scanning. 0x0000 disables timeout. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)     /**< Minimum acceptable connection interval (20 ms). Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(75, UNIT_1_25_MS)     /**< Maximum acceptable connection interval (75 ms). Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                   /**< Slave latency. */

#define DEAD_BEEF                       0xDEADBEEF                          /**< Value used as error code on stack dump. Can be used to identify stack location on stack unwind. */

#define UART_TX_BUF_SIZE                256                                 /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                 /**< UART RX buffer size. */

NRF_BLE_SCAN_DEF(m_scan);                                                   /**< Scanning module instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                   /**< GATT module instance. */

static char const m_target_periph_name[] = "Voi_Scooter_IoT";     /**< Name of the device we try to find. This name is searched in the scan report data*/
// BLE DEFINES END

uint8_t flagScan = 1;                   /**< Flag to know the State of the Scan*/
uint8_t counter = 0;                    /**< Counter number of different Scooters found */
ble_gap_addr_t scannedAddresses[50];    /**< Array of all the different Addresses found*/


/**
 * @brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of an assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/** @brief Function for initializing the timer module. */
static void timers_init(void)
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
    err_code = app_timer_create(&m_blink_scan, APP_TIMER_MODE_REPEATED, blink_handler); //Match the timer with the handler
    APP_ERROR_CHECK(err_code);
}

/**@brief Function to start scanning.
 */
static void scan_start(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_scan_start(&m_scan);
    APP_ERROR_CHECK(err_code);

    bsp_board_led_off(CENTRAL_MATCH_LED);   //Turn off the Match LED
    bsp_board_led_on(CENTRAL_SCANNING_LED); //Start with the LED on
    ret_code_t ret = app_timer_start(m_blink_scan, 
                                      APP_TIMER_TICKS(LED_BLINK_INTERVAL),
                                      (void *) CENTRAL_SCANNING_LED); //Start the timer
    APP_ERROR_CHECK(ret);

}

/**
 * @brief Function for putting the chip into sleep mode.
 *
 * @note This function does not return.
 */
static void sleep_mode_enter(void)
{
    uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}

/**
 * @brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint32_t err_code;

    // For readability.
    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;


    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST:
        {
            
        } break;

        case BLE_GAP_EVT_TIMEOUT:
        {
            // We have not specified a timeout for scanning, so only connection attemps can timeout.
            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
            {
                NRF_LOG_DEBUG("Connection request timed out.");
            }
        } break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
        {
            // Accept parameters requested by peer.
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
                                        &p_gap_evt->params.conn_param_update_request.conn_params);
            APP_ERROR_CHECK(err_code);
        } break;


        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}

/**
 * @brief Function for the SoftDevice initialization.
 *
 * @details This function initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/** @brief Function for initializing the GATT library. */
void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);

}


/**
 * @brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
    uint32_t err_code;
    uint8_t frame_counter;
    switch (event)
    {
        case BSP_EVENT_SLEEP:
            sleep_mode_enter();
            break;

        case BSP_EVENT_KEY_0: //Button 0 pushed
            if(flagScan)
            {
              scan_start();
              flagScan = 0;
            }
            else
            {
              nrf_ble_scan_stop(); //STOP Scan
              ret_code_t ret = app_timer_stop(m_blink_scan); //Turn off the Timer for the LED
              APP_ERROR_CHECK(ret);
              bsp_board_led_off(CENTRAL_SCANNING_LED); //Turn off LEDs
              bsp_board_led_off(CENTRAL_MATCH_LED);
              flagScan = 1; // Reset the flagScan to 1

              size_t size = sprintf(m_tx_buffer, "Found %02i devices \r\n\0", counter); //Print in USB
              app_usbd_cdc_acm_write(&m_app_cdc_acm, m_tx_buffer, size);
              
              NRF_LOG_INFO("Found %02i devices", counter); //Print the Result in LOG
              
              counter = 0;  //Restart Counter for next scan and clear the first Address
              scannedAddresses[counter].addr[0]= 0;
              scannedAddresses[counter].addr[1]= 0;
              scannedAddresses[counter].addr[2]= 0;
              scannedAddresses[counter].addr[3]= 0;
              scannedAddresses[counter].addr[4]= 0;
              scannedAddresses[counter].addr[5]= 0;
            }
            break;
        default:
            break;
    }
}

/**@brief Function for handling Scaning events.
 *
 * @param[in]   p_scan_evt   Scanning event.
 */
static void scan_evt_handler(scan_evt_t const * p_scan_evt)
{
    ret_code_t err_code;

    switch(p_scan_evt->scan_evt_id)
    {
        case NRF_BLE_SCAN_EVT_FILTER_MATCH:
            bsp_board_led_on(CENTRAL_MATCH_LED);
            //Print Address
//            NRF_LOG_INFO("Scanned target %02x:%02x:%02x:%02x:%02x:%02x",
//                      p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[5],
//                      p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[4],
//                      p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[3],
//                      p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[2],
//                      p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[1],
//                      p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[0]
//                      );

            //Compare the received address with all the received addresses
            if(counter < 50)  //Max limit of addresses
            {
              uint8_t i = 0;
              uint8_t flagFound = 0; //Flag to know if we found the Address in the Array
              for(i = 0; i <= counter; i++)
              {
                if(scannedAddresses[i].addr[0]== p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[0] &&
                   scannedAddresses[i].addr[1]== p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[1] &&
                   scannedAddresses[i].addr[2]== p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[2] &&
                   scannedAddresses[i].addr[3]== p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[3] &&
                   scannedAddresses[i].addr[4]== p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[4] &&
                   scannedAddresses[i].addr[5]== p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[5])
                {
                   flagFound = 1; //If found set the flag to 1
                   break;
                }
              }

              if(flagFound == 0) //Did not found the device in our registered addresses
              {
                   counter++;
                   if(counter <= 50) //Save the new address at the end
                   {
                     scannedAddresses[counter-1].addr[0]= p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[0];
                     scannedAddresses[counter-1].addr[1]= p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[1];
                     scannedAddresses[counter-1].addr[2]= p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[2];
                     scannedAddresses[counter-1].addr[3]= p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[3];
                     scannedAddresses[counter-1].addr[4]= p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[4];
                     scannedAddresses[counter-1].addr[5]= p_scan_evt->params.filter_match.p_adv_report->peer_addr.addr[5];
                   }
              }
            }
            else //above or equal to 50 
            {
              nrf_ble_scan_stop();  //STOP SCAN
              ret_code_t ret = app_timer_stop(m_blink_scan); //Turn off the Timer for the LED
              APP_ERROR_CHECK(ret);
              bsp_board_led_off(CENTRAL_SCANNING_LED); //Turn off the LEDs
              bsp_board_led_off(CENTRAL_MATCH_LED);
              flagScan = 1; //Reset the flagScan
              NRF_LOG_INFO("Found %02i devices", counter);

              counter = 0;  //Counter to 0 for next scan
              scannedAddresses[counter].addr[0]= 0; //Clear contents of the first registered Addreses for next round
              scannedAddresses[counter].addr[1]= 0;
              scannedAddresses[counter].addr[2]= 0;
              scannedAddresses[counter].addr[3]= 0;
              scannedAddresses[counter].addr[4]= 0;
              scannedAddresses[counter].addr[5]= 0;
            }
            break;

        case NRF_BLE_SCAN_EVT_CONNECTING_ERROR:
            err_code = p_scan_evt->params.connecting_err.err_code;
            APP_ERROR_CHECK(err_code);
            break;
        default:
          break;
    }
}

/** @brief Function for initializing buttons and LEDs. */
static void buttons_leds_init(void)
{
    uint32_t err_code = bsp_init(BSP_INIT_LEDS, bsp_event_handler); //Init LEDs
    APP_ERROR_CHECK(err_code);

    err_code = bsp_init(BSP_INIT_BUTTONS, bsp_event_handler); //Init and Initialize Buttons
    APP_ERROR_CHECK(err_code);
}


/** @brief Function for initializing the nrf_log module. */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/** @brief Function for placing the application in low power state while waiting for events. */
static void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

static void scan_init(void)
{
    ret_code_t          err_code;
    nrf_ble_scan_init_t init_scan;

    memset(&init_scan, 0, sizeof(init_scan));

    init_scan.connect_if_match = false; //Do not connect if match as we only want to filter
    init_scan.conn_cfg_tag     = APP_BLE_CONN_CFG_TAG;

    err_code = nrf_ble_scan_init(&m_scan, &init_scan, scan_evt_handler);
    APP_ERROR_CHECK(err_code);

    // Setting filters for scanning.
    err_code = nrf_ble_scan_filters_enable(&m_scan, NRF_BLE_SCAN_NAME_FILTER, false);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_scan_filter_set(&m_scan, SCAN_NAME_FILTER, m_target_periph_name);
    APP_ERROR_CHECK(err_code);
}

/**
 * @brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void)
{
    UNUSED_RETURN_VALUE(NRF_LOG_PROCESS());
    power_manage();
}

// USB CODE START

/** @brief User event handler @ref app_usbd_cdc_acm_user_ev_handler_t */
static void cdc_acm_user_ev_handler(app_usbd_class_inst_t const * p_inst,
                                    app_usbd_cdc_acm_user_event_t event)
{
    app_usbd_cdc_acm_t const * p_cdc_acm = app_usbd_cdc_acm_class_get(p_inst);

    switch (event)
    {
        case APP_USBD_CDC_ACM_USER_EVT_PORT_OPEN:
        {
            /*Set up the first transfer*/
            ret_code_t ret = app_usbd_cdc_acm_read(&m_app_cdc_acm,
                                                   m_rx_buffer,
                                                   1);
            UNUSED_VARIABLE(ret);
            NRF_LOG_INFO("CDC ACM port opened");
            break;
        }

        case APP_USBD_CDC_ACM_USER_EVT_PORT_CLOSE:
            NRF_LOG_INFO("CDC ACM port closed");
            break;

        case APP_USBD_CDC_ACM_USER_EVT_TX_DONE:
            break;

        case APP_USBD_CDC_ACM_USER_EVT_RX_DONE:
        {
            ret_code_t ret;
            
            do
            {
                /*Get amount of data transferred*/
                size_t size = app_usbd_cdc_acm_rx_size(p_cdc_acm);
                NRF_LOG_DEBUG("RX: size: %lu char: %c", size, m_rx_buffer[0]);

                /* Fetch data until internal buffer is empty */
                ret = app_usbd_cdc_acm_read(&m_app_cdc_acm,
                                            m_rx_buffer,
                                            1);
            }
            while (ret == NRF_SUCCESS);

            break;
        }
        default:
            break;
    }
}

static void usbd_user_ev_handler(app_usbd_event_type_t event)
{
    switch (event)
    {
        case APP_USBD_EVT_DRV_SUSPEND:
            break;

        case APP_USBD_EVT_DRV_RESUME:
            break;

        case APP_USBD_EVT_STARTED:
            break;

        case APP_USBD_EVT_STOPPED:
            app_usbd_disable();
            break;

        case APP_USBD_EVT_POWER_DETECTED:
            NRF_LOG_INFO("USB power detected");

            if (!nrf_drv_usbd_is_enabled())
            {
                app_usbd_enable();
            }
            break;

        case APP_USBD_EVT_POWER_REMOVED:
        {
            NRF_LOG_INFO("USB power removed");
            app_usbd_stop();
        }
            break;

        case APP_USBD_EVT_POWER_READY:
        {
            NRF_LOG_INFO("USB ready");
            app_usbd_start();
        }
            break;

        default:
            break;
    }
}

// USB CODE END

/** @brief Application main function. */
int main(void)
{
    ret_code_t ret;
    static const app_usbd_config_t usbd_config = {
        .ev_state_proc = usbd_user_ev_handler
    };
    // Initialize.
    log_init();
    timers_init();

    buttons_leds_init();

    app_usbd_serial_num_generate();

    ret = nrf_drv_clock_init();
    APP_ERROR_CHECK(ret);

    //Start the PROGRAM
    NRF_LOG_INFO("VOI TEST started.");

    ret = app_usbd_init(&usbd_config);
    APP_ERROR_CHECK(ret);

    app_usbd_class_inst_t const * class_cdc_acm = app_usbd_cdc_acm_class_inst_get(&m_app_cdc_acm);
    ret = app_usbd_class_append(class_cdc_acm);
    APP_ERROR_CHECK(ret);

    ble_stack_init();
    scan_init();
    gatt_init();
    
    ret = app_usbd_power_events_enable();
    APP_ERROR_CHECK(ret);

    // Enter main loop.
    for (;;)
    {
        while (app_usbd_event_queue_process())
        {
            /* Nothing to do */
        }
        idle_state_handle();
    }
}

/**
 * @}
 */
