# VOI_TEST

Test to scan for VOI Scooters in the vecinity. 

As of now the code is able to:
*  Start to scan the VOI scooters in the vecinity by pressing the button.
*  The LED starts blinking every second when the scan is in progress.
*  Stops scanning by pressing the button again.
*  Stops scanning by finding more than 50 scooters.
*  Counts the numbers of different scooters scanned.
*  Prints the counter as a LOG Entry. This was tested succesfully in the nRF52840-DK board 
*  Creates a USB-Serial Port.
*  Print the counter in the USB-Virtual COM.
  

Note: The USB refused to work so I created a new project and started testing part by part. At the end it worked, it must have been a configuration problem.
------------------------------------------------------------------------------------------------
The code is based in the example codes, BLE Client and USB BLE CDC, provided in the Nordic SDK.
Segger Studio was used as the IDE to write the code.

To program the Dongle: 

1.  Open nRF Connect
2.  Open the programmer 
3.  Select the Dongle 
3.  Select the softdevice hex along with the release hex.
4.  Write the Code.

After it is programmed the Dongle will create a USB Virtual Serial COM.
Connect to it with the following parameters.
*  Baud Rate: 115200
*  Data bits: 8
*  Parity: None
*  Stopbits: 1
*  Handshake: None

The DTR must be set to see the data.